<?php
	//fetches an xml document using the cURL library
	function fetchXML($url)
	{
		//initialize library
		$ch = curl_init();

		//used to make us look like a browser
		$useragent="Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1";

		//set the url
		curl_setopt ($ch, CURLOPT_URL, $url);

		//set that we want to wait for a response
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);

		//make us look like a web browser
		curl_setopt ($ch, CURLOPT_USERAGENT, $useragent);

		//get data
		$data = curl_exec($ch);

		//clean up
		curl_close($ch);

		///return xml data
		return $data;
	}
	define("USE_CURL", true);
	$url = "http://www.wowarmory.com/character-sheet.xml?r=". $_GET['r'] ."&n=". $_GET['cn'];

	//get xml doc with character info
	$data = fetchXML($url);

	//create a SimpleXML object to parse the xml
	$char_xml = new SimpleXmlElement($data);

	//Print basic character info
	$Name = "Name: ". $char_xml->characterInfo->character->attributes()->name;
	$Realm = "Realm: ". $char_xml->characterInfo->character->attributes()->realm;
	$Class = "Class: ". $char_xml->characterInfo->character->attributes()->class;
	$Faction = "Faction: ". $char_xml->characterInfo->character->attributes()->faction;
	$Level = "Level: ". $char_xml->characterInfo->character->attributes()->level;
	$Race = "Race: ". $char_xml->characterInfo->character->attributes()->race;
	$factionName = $char_xml->characterInfo->character->attributes()->faction;

	//Specify constant values 
	$width = 400; //Image width in pixels
	$height = 50; 

	//Create the image resource 
	$image = ImageCreate($width, $height); 

	//We are making four colors, white, black, blue and red 
	$White = imageColorAllocate($image, 255, 255, 255);

	if($factionName == "Horde"){
		$factionColor = imagecolorallocate($image, 255, 0, 0);
		$BG = imagecolorallocate($image, 0, 0, 0);
	}else{
		$factionColor = imagecolorallocate($image, 0, 0, 255);
		$BG = imagecolorallocate($image, 255, 255, 255);
	}

	//Create image background 
	ImageFill($image, $width, $height, $BG);
	ImageFilledRectangle($image, 0, 0, $width-1, $height-1, $BG);
	if($factionName == "Horde"){
		imageLine($image, 0, 0, 400, 0, imagecolorallocate($image, 255, 0, 0));
		imageLine($image, 0, 0, 0, 50, imagecolorallocate($image, 255, 0, 0));
		imageLine($image, $width-1, 0, $width-1, 50, imagecolorallocate($image, 255, 0, 0));
		imageLine($image, 0, $height-1, 400, $height-1, imagecolorallocate($image, 255, 0, 0));
	}else{
		imageLine($image, 0, 0, 400, 0, imagecolorallocate($image, 0, 0, 255));
		imageLine($image, 0, 0, 0, 50, imagecolorallocate($image, 0, 0, 255));
		imageLine($image, $width-1, 0, $width-1, 50, imagecolorallocate($image, 0, 0, 255));
		imageLine($image, 0, $height-1, 400, $height-1, imagecolorallocate($image, 0, 0, 255));
	}
	imageString($image, 3, 10, 10,  $Name, $factionColor);
	imageString($image, 3, 10, 26,  $Realm, $factionColor);
	imageString($image, 3, 150, 26,  $Class, $factionColor);
	imageString($image, 3, 275, 10,  $Faction, $factionColor);
	imageString($image, 3, 310, 26,  $Level, $factionColor);
	imageString($image, 3, 150, 10,  $Race, $factionColor);

	//Tell the browser what kind of file is come in 
	header("Content-Type: image/png"); 

	//Output the newly created image in jpeg format 
	ImagePNG($image);

	//Free up resources 
	ImageDestroy($image);



?>		